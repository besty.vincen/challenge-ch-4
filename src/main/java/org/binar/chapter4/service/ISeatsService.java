package org.binar.chapter4.service;

import org.binar.chapter4.model.seats;

import java.util.List;

public interface ISeatsService {

    List<seats> showSeats();
}
